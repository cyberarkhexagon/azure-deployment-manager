TRUE="true"
FALSE="false"


# Args:
# $1 message (string)
function PrintInfo()
{
    Print "INFO" 3 "$1"
}

# Args:
# $1 message (string)
function PrintError()
{
    Print "ERROR" 1 "$1"
}

# Args:
# $1 message (string)
function PrintDebug()
{
    if [ "$VERBOSE_MODE" = $TRUE ]
    then
        Print "DEBUG" 4 "$1"
    fi
}

# Args:
# $1 type    (string)
# $2 color   (int)
# $3 message (string)
function Print()
{
    tput setaf $2 && cat << EOF
$1: $3
EOF
}

function IsArrayWithItems
{
    EXIT_CODE="0"
    IS_NOT_EMPTY_ARRAY=$(echo "$1" | jq ". | if type == \"array\" and length > 0 then $TRUE else $FALSE end") || EXIT_CODE=$?
    if [ $EXIT_CODE == "0" ] && [ $IS_NOT_EMPTY_ARRAY == $TRUE ]
    then
        echo $TRUE
    else
        echo $FALSE
    fi
}


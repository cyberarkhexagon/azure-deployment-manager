#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source ./helpers.sh

APP_ID=
PASSWORD=
APP_DISPLAY_NAME=
APP_REPLY_URLS=
BASE64_STRING=
SUBSCRIPTIONS_TO_ASSIGN_CEM_ROLE=
MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE=
IDEMPOTENT_MODE=$FALSE
VERBOSE_MODE=$FALSE

DIRECTORY_ID=$(az account show | jq -r '.tenantId')
APP_SECRET_END_DATE=$(date -d "+2 years" +"%Y-%m-%d") # 2 years from now
DEFAULT_APP_REPLY_URL="https://cem.cyberark.com/azure-admin-consent"
CEM_CUSTOM_ROLE_NAME="CyberArk-CEM-Application-Role"
CEM_CUSTOM_ROLE_ASSIGNABLE_SCOPE="/providers/Microsoft.Management/managementGroups/$DIRECTORY_ID"

function ShowUsage
{
    echo "Usage: $0 [-n APPLICATION_DISPLAY_NAME or -a EXISTING_APPLICATION_ID]"
    echo "Optional flags:"
    echo "-n \"Application display name"
    echo "-a \"Application id (use when it already exists)"
    echo "-s \"SUBSCRIPTION_ID_TO_ASSIGN_READER_ROLE_TO_1,SUBSCRIPTION_ID_TO_ASSIGN_READER_ROLE_TO_2\""
    echo "-m \"MANAGEMENT_GROUP_ID_TO_ASSIGN_READER_ROLE_TO_1,MANAGEMENT_GROUP_ID_TO_ASSIGN_READER_ROLE_TO_2\" (when set to \"root\" assigns the root management group)"
    echo "-i \"Idempotent mode - doesn't create new app credential if it already exists\""
    echo "-r \"Application reply URLs (separated by space)\""
    echo "-v \"Verbose mode\""
}

while getopts n:a:r:s:m:ivh o
do  case "$o" in
  n) APP_DISPLAY_NAME="$OPTARG";;
  a) APP_ID="$OPTARG";;
  r) APP_REPLY_URLS="$OPTARG";;
  s) SUBSCRIPTIONS_TO_ASSIGN_CEM_ROLE="$OPTARG";;
  m) MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE="$OPTARG";;
  i) IDEMPOTENT_MODE=$TRUE;;
  v) VERBOSE_MODE=$TRUE;;
  [?] | h) ShowUsage ; exit 1;;
  esac
done


if [[ "${APP_DISPLAY_NAME}" == "APPLICATION_DISPLAY_NAME" ]]; then
  ShowUsage
  exit 1
fi

if [[ -z "${APP_DISPLAY_NAME}" ]] && [[ -z "${APP_ID}" ]]; then
  ShowUsage
  exit 1
fi

function jsonToBase64
{
    JSON_STRING=$( jq -n \
                  --arg di "$DIRECTORY_ID" \
                  --arg ai "$APP_ID" \
                  --arg pa "$PASSWORD" \
                  '{directory_id: $di, application_id: $ai, secret: $pa}')
                  
    BASE64_STRING=$(echo $JSON_STRING | base64)   
}

function PrintResult
{
    if [ "$PASSWORD" != "" ]
    then

        # On first run of the script, when app and credentials were created, return the final value
        jsonToBase64
        echo "finalValue:"
        tput setaf 2 && cat << EOF
${BASE64_STRING}
EOF
    else

        # Should get here when running the script multiple times. Where app and credential already exist
        tput setaf 2 && cat << EOF
Successfully finished!
EOF
    fi
}

function CreateCEMCustomRole()
{
    PrintInfo "Creating/validating existence of CEM Custom role"

    ROLE_DEFINITION='{
        "Name": "'${CEM_CUSTOM_ROLE_NAME}'",
        "Description": "The permissions needed for CyberArk CEM",
        "Actions": [
            "*/read",
            "Microsoft.Web/sites/publish/Action"
        ],
        "DataActions": [
        ],
        "NotDataActions": [
        ],
        "AssignableScopes": ["'${CEM_CUSTOM_ROLE_ASSIGNABLE_SCOPE}'"]
    }'

    # Try to get CEM custom role if it already exists
    declare -i SUBSCRIPTIONS_COUNT
    PrintDebug "Getting all existing subscriptions"
    SUBSCRIPTIONS_AS_ARRAY=$(az account list | jq [.[].id])
    SUBSCRIPTIONS_COUNT=$(az account list | jq [.[].id] | wc -l)
    SUBSCRIPTIONS_COUNT=$((SUBSCRIPTIONS_COUNT - 3))
    PrintDebug "Listing all CEM role definitions"
    LIST_ROLE_DEFINITION_RES=
    for id_in_count in $(seq 0 $SUBSCRIPTIONS_COUNT)
    do
        SUBSCRIPTION_ID=$(echo $SUBSCRIPTIONS_AS_ARRAY | jq .[$id_in_count])
        SUBSCRIPTION_ID=${SUBSCRIPTION_ID%?}
        SUBSCRIPTION_ID=${SUBSCRIPTION_ID#?}

        # Try to get role definition for this subscription
        EXIT_CODE="0"
        LIST_ROLE_DEFINITION_RES=$(az role definition list --name ${CEM_CUSTOM_ROLE_NAME} --subscription $SUBSCRIPTION_ID) || EXIT_CODE=$?
        if [ $EXIT_CODE == "0" ] && [ $(IsArrayWithItems "$LIST_ROLE_DEFINITION_RES") == $TRUE ]
        then
            break
        fi
    done
    PrintDebug "CEM role definitions result: $LIST_ROLE_DEFINITION_RES"
    if [ $(IsArrayWithItems "$LIST_ROLE_DEFINITION_RES") == $TRUE ]
    then
        ROLE_ID=$(echo $LIST_ROLE_DEFINITION_RES | jq ".[0].id")
        PrintDebug "Existing CEM Custom role ID: $ROLE_ID"

        PREVIOUS_ASSIGNABLE_SCOPES=$(echo $LIST_ROLE_DEFINITION_RES | jq ".[0].assignableScopes")

        if [[ $PREVIOUS_ASSIGNABLE_SCOPES != *"$CEM_CUSTOM_ROLE_ASSIGNABLE_SCOPE"* ]]
        then
            PrintInfo "CEM custom role already exists. Updating assignable scopes"

            # Update role definition to match the Update schema
            UPDATE_ROLE_DEFINITION=$ROLE_DEFINITION
            UPDATE_ROLE_DEFINITION=$(echo $UPDATE_ROLE_DEFINITION | jq ".roleName |= . + \"$CEM_CUSTOM_ROLE_NAME\"")
            UPDATE_ROLE_DEFINITION=$(echo $UPDATE_ROLE_DEFINITION | jq ".id |= . + $ROLE_ID")
            EXIT_CODE=0
            RES=$(az role definition update --role-definition "$UPDATE_ROLE_DEFINITION" 2>&1) || EXIT_CODE=$?
            if [ $EXIT_CODE != 0 ] ; then
                PrintError "Unkown error while updating CEM custom role"
                PrintError "$RES"
                exit $EXIT_CODE
            fi
            WaitForCustomRoleToStabilize
            return
        else
            PrintDebug "CEM custom role already exists. Skipping creation"
            return
        fi
    fi

    EXIT_CODE=0
    PrintDebug "Creating CEM custom role"
    RES=$(az role definition create --role-definition "$ROLE_DEFINITION" 2>&1) || EXIT_CODE=$?
    if [ $EXIT_CODE != 0 ] ; then
        PrintError "Unkown error while creating CEM custom role"
        PrintError "$RES"
        exit $EXIT_CODE
    fi

    WaitForCustomRoleToStabilize
}

function WaitForCustomRoleToStabilize
{
    # TODO: replace it in the future if needed with polling
    sleep 5
}

function CreateResourceFile
{
cat > requiredResourceManifest.json << EOF
    [
        {
            "resourceAppId": "00000003-0000-0000-c000-000000000000",
            "resourceAccess": [
                {
                    "id": "b0afded3-3588-46d8-8b3d-9842eff778da",
                    "type": "Role"
                },
                {
                    "id": "7ab1d382-f21e-4acd-a863-ba3e13f7da61",
                    "type": "Role"
                },
                {
                    "id": "483bed4a-2ad3-4361-a73b-c83ccdbdc53c",
                    "type": "Role"
                }
            ]
        }
    ]
EOF
}

function RunDeploymentSteps
{
    # Create a custom role needed for CEM's application
    CreateCEMCustomRole

    if [ "$APP_ID" == "" ]
    then

        # Create an application
        PrintInfo "Creating CEM Application"
        APP_CREATION_CMD="az ad app create --display-name ${APP_DISPLAY_NAME} \
        --required-resource-accesses \"requiredResourceManifest.json\" \
        --end-date ${APP_SECRET_END_DATE} "
        if [ "$APP_REPLY_URLS" == "" ]
        then
            APP_REPLY_URLS=$DEFAULT_APP_REPLY_URL
        fi
        APP_CREATION_CMD="${APP_CREATION_CMD} --web-redirect-uris \"${APP_REPLY_URLS}\""
        APP_CREATION_CMD="${APP_CREATION_CMD} | jq '.appId' | sed -e 's/^\"//' -e 's/\"$//'"
        APP_ID=$(eval ${APP_CREATION_CMD})
        PrintInfo "Successfully created CEM application. appId: $APP_ID"
    else
        PrintInfo "CEM Application already exists. appId: $APP_ID"
    fi

    GenerateAppCredential

    # Create a service principal for that application
    CreateAppServicePrincipal

    # Assign reader role to the application
    # (Current usage is to assign either to multiple subscriptions or to the root management group)

    # Subscriptions
    if [ "$SUBSCRIPTIONS_TO_ASSIGN_CEM_ROLE" != "" ]
    then
        AssignCEMRoleToSubscriptions "$SUBSCRIPTIONS_TO_ASSIGN_CEM_ROLE"
    fi

    # Management Groups
    if [ "$MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE" != "" ]
    then
        if [ "$MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE" == "root" ]
        then
            MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE=$DIRECTORY_ID
        fi

        AssignCEMRoleToManagementGroups "$MANAGEMENT_GROUPS_TO_ASSIGN_CEM_ROLE"
    fi
}

function GenerateAppCredential
{
    PrintInfo "Generating new app credential if needed"

    # List existing app credentials
    LIST_APP_CREDENTIAL_RES=$(az ad app credential list --id ${APP_ID})
    if [ $(IsArrayWithItems "$LIST_APP_CREDENTIAL_RES") == $TRUE ]
    then
        PrintInfo "Application credential already exists"
        if [ "$IDEMPOTENT_MODE" = $TRUE ]
        then
            PrintInfo "Continuing without generating a new credential"
            return
        else
            DeleteExistingAppCredentials
        fi
    fi

    # Generate a credential for the application
    PASSWORD=$(az ad app credential reset --id ${APP_ID} --append --end-date ${APP_SECRET_END_DATE} | jq '.password' | sed -e 's/^"//' -e 's/"$//')
}

function DeleteExistingAppCredentials
{
    PrintInfo "Deleting existing app credentials"

    GET_APP_CREDENTIAL_ID_CMD="az ad app credential list --id ${APP_ID} | jq .[0].keyId | sed 's/\"//g'"
    CURRENT_APP_CREDENTIAL_ID=$(eval ${GET_APP_CREDENTIAL_ID_CMD})

    EXIT_CODE=0
    while [ "$CURRENT_APP_CREDENTIAL_ID" != "null" ]
    do
        RES=$(az ad app credential delete --id ${APP_ID} --key-id ${CURRENT_APP_CREDENTIAL_ID} 2>&1) || EXIT_CODE=$?
        if [ $EXIT_CODE != 0 ]
        then
            PrintError "Unkown error while deleting existing app credential: ${CURRENT_APP_CREDENTIAL_ID}"
            PrintError "$RES"
            exit $EXIT_CODE
        fi

        # Try to get the next app credential if such exists
        CURRENT_APP_CREDENTIAL_ID=$(eval ${GET_APP_CREDENTIAL_ID_CMD})
    done

    PrintInfo "Successfully deleted existing app credentials"
}

# Create a service principal for our application (so we'll be able to assign roles to it)
function CreateAppServicePrincipal
{
    PrintInfo "Creating app service principal if needed"
    EXIT_CODE=0

    RES=$(az ad sp create --id ${APP_ID} 2>&1) || EXIT_CODE=$?
    if [ $EXIT_CODE != 0 ] ; then
        if [[ $RES =~ "already exists" ]] || [[ $RES =~ "already in use" ]]
        then
            PrintDebug "Service principal already exists. Continuing deployment normally"
        else
            PrintError "Unkown error while creating service principal for the application"
            PrintError "${RES}"
            exit $EXIT_CODE
        fi
    fi
}

# Receives management group id as argument
function AssignCEMRoleToManagementGroup()
{
    EXIT_CODE=0

    RES=$(az role assignment create \
        --assignee ${APP_ID} \
        --role "${CEM_CUSTOM_ROLE_NAME}" \
        --scope "/providers/Microsoft.Management/managementGroups/$1" 2>&1) || EXIT_CODE=$?
    if [ $EXIT_CODE != 0 ] ; then
        PrintError "Unkown error while assigning CEM's role to \"$1\" management group"
        PrintError "${RES}"
        exit $EXIT_CODE
    fi
}

# Receives subscription id as argument
function AssignCEMRoleToSubscription()
{
    EXIT_CODE=0
    RES=$(az role assignment create \
        --assignee ${APP_ID} \
        --role "${CEM_CUSTOM_ROLE_NAME}" \
        --subscription "$1" 2>&1) || EXIT_CODE=$?
    if [ $EXIT_CODE != 0 ] ; then
        PrintError "Unkown error while assigning CEM's role to "$1" subscription"
        PrintError "${RES}"
        exit $EXIT_CODE
    fi
}

# Receives subscription IDs string separated by spaces, and assigns each with a reader role
# example: `AssignCEMRoleToSubscriptions "sub-1,sub-2,sub-3"`
function AssignCEMRoleToSubscriptions()
{
    PrintInfo "Assigning CEM role to subscriptions: $1"
    for subscription in $(echo $1 | tr "," "\n")
    do
        AssignCEMRoleToSubscription "$subscription"
    done
}

# Receives management group IDs string separated by spaces, and assigns each with a reader role
# example: `AssignCEMRoleToManagementGroups "mg-1,mg-2,mg-3"`
function AssignCEMRoleToManagementGroups()
{
    PrintInfo "Assigning CEM role to management groups: $1"
    for management_group in $(echo $1 | tr "," "\n")
    do
        AssignCEMRoleToManagementGroup "$management_group"
    done
}

CreateResourceFile
RunDeploymentSteps
PrintResult
